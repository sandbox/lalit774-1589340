<?php
/*
 * user specific language settings form.
 */
function user_specific_language_settings_form() {
  $form['user_specific_language_on_login'] = array(
    '#type'     => 'checkbox',
    '#title' => t('Language Option show on login page.'),
    '#default_value' => variable_get("user_specific_language_on_login", 0),
  );
  $form['user_specific_language_on_register'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Language Option show on register page'),
    '#default_value' => variable_get("user_specific_language_on_register", 0),
  ); 
  return system_settings_form($form);
}

?>
